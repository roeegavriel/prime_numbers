package rg.prime_numers.calculator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Calculator {

    public static Map<Long, List<Long>> findPrimeNumbers(Long max, Consumer<Long> onFound) {
        Long candidate = 2L;
        Map<Long, List<Long>> primeNumbersNextSteps = new HashMap<>();
        return calc(candidate, primeNumbersNextSteps, max, onFound);
    }

    public static Map<Long, List<Long>> calc(Long firstCandidate, Map<Long, List<Long>> primeNumbersNextSteps, Long max, Consumer<Long> onFind) {
        Long candidate = firstCandidate;
        for (; candidate <= max; ++candidate) {
            if (isCandidatePrimeNumber(primeNumbersNextSteps, candidate)) {
                onFind.accept(candidate);
                handleCandidatePrime(primeNumbersNextSteps, candidate);
            } else {
                handleCandidateNotPrime(primeNumbersNextSteps, candidate);
            }
        }
        return primeNumbersNextSteps;
    }

    private static boolean isCandidatePrimeNumber(Map<Long, List<Long>> primeNumbersNextSteps, Long candidate) {
        return !primeNumbersNextSteps.containsKey(candidate);
    }

    private static void handleCandidatePrime(Map<Long, List<Long>> primeNumbersNextSteps, Long candidate) {
        addPrimeNextStep(primeNumbersNextSteps, candidate, candidate * candidate);
    }

    private static void handleCandidateNotPrime(Map<Long, List<Long>> primeNumbersNextSteps, Long candidate) {
        List<Long> currentSteps = primeNumbersNextSteps.get(candidate);
        for (Long prime : currentSteps) {
            addPrimeNextStep(primeNumbersNextSteps, prime, candidate + prime);
        }
        primeNumbersNextSteps.remove(candidate);
    }

    private static void addPrimeNextStep(Map<Long, List<Long>> primeNumbersNextSteps, Long prime, Long nextStep) {
        List<Long> nextStepsList = primeNumbersNextSteps.computeIfAbsent(nextStep, k -> new LinkedList<>());
        nextStepsList.add(prime);
    }
}
