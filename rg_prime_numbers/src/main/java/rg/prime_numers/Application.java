package rg.prime_numers;

import rg.prime_numers.calculator.Calculator;

import java.util.concurrent.atomic.AtomicInteger;

public class Application {
    public static void main(String[] args) {
        System.out.println("Starting...");
        AtomicInteger count = new AtomicInteger(0);
        Calculator.findPrimeNumbers(1000000L, prime -> {
            count.incrementAndGet();
            System.out.println(prime);
        });
        System.out.println("Found " + count.get() + " prime numbers.");
        System.out.println("Done!");
    }
}