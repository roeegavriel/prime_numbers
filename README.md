# Roee Gavriel's Prime Numbers algorithm
This is an implementation to an improvement of `Sieve of Eratosthenes`.

Instead of working with a predefined array, this algorithm works iteratively with storing each found prime number in a 
list in the map where the key is the prime next iteration step.
To put things in simple words, when `sieve of Eratosthenes` algorithm encounters a prime number, lets say 2, it go over
all it's multiplications 4, 6, 8 etc... and mark them as not a prime numbers.
When `Roee Gavriel's Prime Numbers` algorithm, encounters a prime number, lets take 2 again, it will only add the 
number 2 to a bucket with a key of 4. Later when it will get to 4, it will know 4 is not a prime number because its 
bucket is not empty and will move the number 2 to the bucket of 6.  

## Comparison
### Time complexity
Both `Sieve of Eratosthenes` and `Roee Gavriel's Prime Numbers` algorithms takes `O(N log (log N))` 
time to find all prime numbers less than N. 
### Memory complexity
While `Sieve of Eratosthenes` required O(N) memory for holding an array in the size of N to find all prime numbers less than N.  
`Roee Gavriel's Prime Numbers` required O(M), while M is the number of prime numbers less than N. M is significantly smaller than N.
(only 78,498 less than 1,000,000 and 664_579 less than 10,000,000)

## Advantages
 - Memory - this algorithm required much less memory than the classical `Sieve of Eratosthenes`
 - Initialization - unlike the `Sieve of Eratosthenes` which required initialization of an array in size N with the value "true",
 here there is only need to initialize an empty map regardless the size of N.
 - Incremental - Here, you don't need to define the max number from the beginning of the run. Moreover, you can "save" a current state and later load it and continue for the same position.
   
## Example of the map of bucket after each iteration of the algorithm
Starting with an empty map of buckets
```
    {
    }
```
2 don't have a bucket (it's a prime number), we add it to bucket 4 (2*2)
```
    {
        "4": [2]
    }
```
3, like 2 before, don't have a bucket (it's a prime number), we add it to bucket 9 (3*3)
```
    {
        "4": [2],
        "9" :[3]
    }
```
4, has a bucket in the map (it's not a prime number), all the prime numbers in it will move to a bucket bigger by their own value. At this point the bucket of 4 can be deleted.
```
    {
        "6": [2],
        "9" :[3]
    }
```
Using the same logic, the next iteration will look like this:
5, prime number
```
    {
        "6": [2],
        "9" :[3],
        "25": [5]
    }
```
6, not prime number
```
    {
        "8": [2],
        "9" :[3],
        "25": [5]
    }
```
7, prime number
```
    {
        "8": [2],
        "9" :[3],
        "25": [5],
        "49": [7]
    }
```
8, not prime number
```
    {
        "9" :[3],
        "10": [2],
        "25": [5],
        "49": [7]
    }
```
9, not prime number
```
    {
        "10": [2],
        "12" :[3],
        "25": [5],
        "49": [7]
    }
```
10, not prime number
```
    {
        "12" :[2, 3],
        "25": [5],
        "49": [7]
    }
```
11, prime number
```
    {
        "12" :[2, 3],
        "25": [5],
        "49": [7],
        "121": [11]
    }
```
12, not prime number
```
    {
        "14" :[2],
        "15" :[3],
        "25": [5],
        "49": [7],
        "121": [11]
    }
```
